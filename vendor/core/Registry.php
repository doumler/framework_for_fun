<?php

namespace vendor\core;

/**
* class Registry offers a container for objects (pattern "registry")
*/

	class Registry {

	public static $objects = [];

	protected static $instance;

	protected function __construct()
	{

				$settings = require ROOT.'/config/settings.php';
				foreach($settings['components'] as $name=> $component)
				{
					Registry::$objects[$name] = new $component;
				}

	}

	public static function instance()
	{
	if(self::$instance===null){
			self::$instance = new self;
		}
		return self::$instance;

	}
/**
*	
*/
	public function __set($name, $value)
	{
	if(!isset(self::$objects[$name])){
	self::$objects[$name] = new $value;
	}
	}


	public function __get($name)
	{

	if(is_object(self::$objects[$name])){
		return self::$objects[$name];
	}

	}
/*
	public function getList()
	{
	return self::$objects;
	}
*/

}




