<?php
namespace vendor\core\base;

 abstract class Controller{
	
	public $route=[];
	
	public $layout;
	
	public $view;
	
	// data set in view by user
	
	public $vars = [];
	
	public function __construct($route){
		
		$this->route = $route;
		// View file gets its name from controllers method (action)
		$this->view = $route['action'];
	}
	
	public function getView(){
		
		$vObj = new View($this->route, $this->layout, $this->view);
		$vObj->render($this->vars);
	}
	
	public function set($vars){
		$this->vars = $vars;
	}
	
	
}


?>