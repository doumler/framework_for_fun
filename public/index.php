<?php
/**
* Here the fun starts
*/
	error_reporting(-1);

	use vendor\core\Router;
	use vendor\core\base\Controller;
	use vendor\core\base\View;

	$query = rtrim($_SERVER['QUERY_STRING'], '/');

	define('www', __DIR__);
	define('CORE', dirname(__DIR__) . '/vendor/core');
	define('ROOT', dirname(__DIR__));
	define('APP', dirname(__DIR__). '/app');
	define('LAYOUT', 'default');
	define('CACHE', dirname(__DIR__).'/tmp/cache');

	require_once "../vendor/libs/functions.php";



	spl_autoload_register(function($class){
	$file = ROOT . "/". str_replace("\\", "/", $class ). ".php";
	if(file_exists($file)){
		require_once $file;
	}
	
	});

	new \vendor\core\App;

	// custom rules
	Router::add('^page/(?P<action>[a-z-]+)/(?P<alias>[a-z-]+)$', ['controller'=>'Page']);
	Router::add('^page/(?P<alias>[a-z-]+)$', ['controller'=>'Page', 'action'=>'view']);

	// default rules
 
	Router::add('^$', ['controller'=>'Main', 'action' => 'index']);
	Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');
 
	Router::dispatch($query);



