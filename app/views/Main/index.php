<div class="container">
	<?php foreach($product as $prod): ?>
		<div class="panel panel-default">
  <div class="panel-heading"><?= $prod['name']?></div>
  <div class="panel-body">
    <?= $prod['content']?>
  </div>
</div>

	<?php endforeach; ?>
</div>
