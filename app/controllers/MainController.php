<?php

namespace app\controllers;
use app\models\Main;
use vendor\core\App;
use vendor\core\Registry;



class MainController extends AppController{
	
	public $layout = 'main';
	
	public function indexAction(){
		
		
	

		$model = new Main;
		//returned data set in cache
		$product = App::$app->cache->get('post');
		if(!$product){
			$product = $model->findAll();
			App::$app->cache->set('post', $product, 300);
		}
		
		
		/*query examples: 

		$post = $model->findOne(9);
		$data = $model->findBySql("SELECT * FROM {$model->table} WHERE name LIKE ? LIMIT 2", ['%om%']);
		$data1 =$model->findLike('ue', 'ort', 'de');
		
		*/
	
		$name = 'Alex';
		$this->set(['name'=>$name, 'product'=>$product]);
	}
}

